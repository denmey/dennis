package de.dis2011;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import de.dis2011.data.Makler;

/**
 * Hauptklasse
 */
public class Main {
	/**
	 * Startet die Anwendung
	 */
	public static void main(String[] args) {
		showMainMenu();
	}

	/**
	 * Zeigt das Hauptmenü
	 */
	public static void showMainMenu() {
		// Menüoptionen
		final int MENU_ESTATE_AGENT = 0;
		final int MENU_ESTATE = 1;
		final int MENU_CONTRACT = 2;
		final int QUIT = 3;

		// Erzeuge Menü
		Menu mainMenu = new Menu("MainMenu");
		mainMenu.addEntry("Estate agent management", MENU_ESTATE_AGENT);
		mainMenu.addEntry("Estate management", MENU_ESTATE);
		mainMenu.addEntry("Contract management", MENU_CONTRACT);
		mainMenu.addEntry("Quit", QUIT);

		// Verarbeite Eingabe
		while (true) {
			int response = mainMenu.show();

			switch (response) {
			case MENU_ESTATE_AGENT:
				askForPw();
				break;
			case MENU_ESTATE:
				showEstateMenu();
				break;
			case MENU_CONTRACT:
				showContractMenu();
				break;
			case QUIT:
				return;
			}
		}
	}

	private static void askForPw() {
		final String PW = "123";

		System.out.println("Please enter your password:");

		BufferedReader stdin = new BufferedReader(new InputStreamReader(
				System.in));

		String input = "";
		int counter = 0;
		while (!PW.equals(input) && counter < 3) {
			try {
				input = stdin.readLine();
				counter++;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (PW.equals(input)) {
			showEstateAgentMenu();
		} else {
			System.out.println("3x wrong pw. Return to main menu.");
		}
	}

	private static void showContractMenu() {
		// TODO Auto-generated method stub

	}

	private static void showEstateMenu() {
		// TODO Auto-generated method stub

	}

	/**
	 * Zeigt die Maklerverwaltung
	 */
	public static void showEstateAgentMenu() {
		// Menüoptionen
		final int NEW_ESTATE_AGENT = 0;
		final int SHOW_ESTATE_AGENTS = 1;
		final int DELETE_ESTATE_AGENTS = 2;
		final int BACK = 3;

		// Maklerverwaltungsmenü
		Menu maklerMenu = new Menu("Estate agent management");
		maklerMenu.addEntry("New estate agent", NEW_ESTATE_AGENT);
		maklerMenu.addEntry("Show estate agents", SHOW_ESTATE_AGENTS);
		maklerMenu.addEntry("Delete estate agent", DELETE_ESTATE_AGENTS);
		maklerMenu.addEntry("Back to main menu", BACK);

		// Verarbeite Eingabe
		while (true) {
			int response = maklerMenu.show();

			switch (response) {
			case NEW_ESTATE_AGENT:
				newEstateAgent();
				break;
			case SHOW_ESTATE_AGENTS:
				showEstateAgentList();
				break;
			case DELETE_ESTATE_AGENTS:
				deleteEstateAgent();
				break;
			case BACK:
				return;
			}
		}
	}

	private static void showEstateAgentList() {
		Makler m = new Makler();
		
		m.showAllEstateAgents();

	}

	/**
	 * Legt einen neuen Makler an, nachdem der Benutzer die entprechenden Daten
	 * eingegeben hat.
	 */
	public static void newEstateAgent() {
		Makler m = new Makler();

		m.setName(FormUtil.readString("Name"));
		m.setAddress(FormUtil.readString("Adresse"));
		m.setLogin(FormUtil.readString("Login"));
		m.setPassword(FormUtil.readString("Passwort"));
		m.save();

		System.out
				.println("Makler mit der ID " + m.getLogin() + " wurde erzeugt.");
	}
	
	public static void changeMakler() {
		Makler m = Makler.load(FormUtil.readInt("Id/Login des zu ändernden Maklers"));

		m.setName(FormUtil.readString("Name"));
		m.setAddress(FormUtil.readString("Adresse"));
		m.setLogin(FormUtil.readString("Login"));
		m.setPassword(FormUtil.readString("Passwort"));
		m.save();

		System.out.println("Makler mit der ID/Login " + m.getLogin()
				+ " wurde geändert.");
	}

	/**
	 * Löscht einen Makler, nachdem der Benutzer die zu löschende Id eingegeben
	 * hat.
	 */
	public static void deleteEstateAgent() {
		String login = FormUtil.readString("Id/Login des zu löschenden Maklers");
		Makler.delete(login);

		System.out.println("Makler mit der ID/Login " + login + " wurde gelöscht.");
	}
}
